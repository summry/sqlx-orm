import time
from decimal import Decimal
from sqlormx import DelFlag, init_snowflake, get_snowflake_id, db
from test.models import Person
from datetime import datetime


def full_test():
    # ------------------------------------------------测试实例方法 ------------------------------------------------
    u = Person(name='张三', age=55, birth_date='1968-10-08', sex=0, grade=1.0, point=20.5, money=854.56)
    rowcount = u.persist()
    assert rowcount > 0, 'persist'
    Person.where(id__ge=0).delete()
    id = u.inst_save()
    # assert Person.count(name='张三') == 1, 'Count not eq 1'
    u1 = Person(id=id, sex=1)
    u2 = u1.load('name', 'age')
    print(u1)
    print(u2)
    u1.load()
    print(u1)
    assert u1 == u2, 'u1 not eq u2'

    u1.name = '李四'
    u1.update()
    u3 = Person.find_by_id(id, 'id', 'name')
    assert u3.name == '李四', 'update '

    u3.logical_delete()
    u4 = Person.find_by_id(id)
    # assert Person.count() == 1, 'delete'
    assert u4.del_flag == 1, 'delete'

    u4.remove()
    # assert Person.count() == 0, 'delete'

    # ------------------------------------------------测试静态方法------------------------------------------------------
    rowcount = Person.insert(name='张三', age=55, birth_date='1968-10-08', sex=0, grade=1.0, point=20.5, money=854.56)
    assert rowcount == 1, 'insert'

    person = Person.create(name='李四', age=55, birth_date='1968-10-08', sex=0, grade=1.0, point=20.5, money=854.56)
    id2 = person.id
    assert id2 > 0, 'create'

    Person.update_by_id(id2, name='王五')
    u5 = Person.find_by_id(id2, 'name')
    assert u5.name == '王五', 'update_by_id'
    u5 = Person.query_by_id(id2, 'name')
    assert u5['name'] == '王五', 'query_by_id'
    u5 = Person.select_by_id(id2, 'name', 'age', 'create_time')
    print(u5)

    Person.logical_delete_by_id(id2)
    u6 = Person.find_by_id(id2, 'del_flag')
    assert u6.del_flag == 1, 'logic_delete_by_id'
    Person.logical_undelete_by_id(id2)
    u6 = Person.find_by_id(id2, 'del_flag')
    assert u6.del_flag == 0, 'logic_delete_by_id'

    Person.update_by_id(id2, del_flag=0)
    u7 = Person.find_by_id(id2, 'del_flag')
    assert u7.del_flag == 0, 'update_by_id'

    persons = Person.find(name='王五')
    # assert len(persons) == 1, 'find'
    persons = Person.query(name='王五')
    # assert len(persons) == 1, 'query'
    persons = Person.select(name='王五')
    # assert len(persons) == 1, 'select'
    persons = Person.find('id', 'name', limit=2)
    assert len(persons) == 2, 'find'
    ids = [person.id for person in persons]
    Person.logical_delete_by_ids(ids=ids, batch_size=1)
    persons2 = Person.find_by_ids(ids, 'del_flag')
    assert len(persons2) == 2, 'logic_delete_by_ids'
    for person in persons2:
        assert person.del_flag == DelFlag.DELETED.value, 'logic_delete_by_ids'

    Person.logical_undelete_by_ids(ids=ids, update_by=11)
    persons2 = Person.find_by_ids(ids, 'del_flag')
    assert len(persons2) == 2, 'logical_undelete_by_ids'
    for person in persons2:
        assert person.del_flag == DelFlag.UN_DELETE.value, 'logic_delete_by_ids'

    print('delete', Person.delete_by_id(id2))
    # assert Person.count() == 1, 'delete_by_id'

    print('delete', Person.delete_by_ids(ids))
    # assert Person.count() == 0, 'delete_by_ids'

    persons = Person.find('id', 'name', 'grade', 'create_time', name__like="王五%", create_time__between=('2023-06-07 10:48:01', '2023-06-07 10:48:06'))
    for person in persons:
        print(person)
    persons = Person.find()
    for person in persons:
        print(person)


    # kwargs = {
    #     'name': '张三',
    #     'age': 18,
    #     'sex': 0
    # }
    # table = 'person'
    # cols, args = zip(*kwargs.items())
    # sql = 'INSERT INTO `%s` (%s) VALUES (%s)' % (table, ','.join(['`%s`' % col for col in cols]), ','.join(['%' + '(%s)' % col + 's' for col in cols]))
    # print(sql)

    # cls = Person.__class__
    # person = cls(('test', Model, kwargs))
    # print(Person)

    # for person in Person.find():
    #     print(person)

    now = datetime.now()
    Person.batch_insert(*[{'name': '王五', 'age': 55, 'birth_date': '1968-10-08', 'sex': 0, 'grade': 1.0, 'point': 20.5, 'money': 854.56, 'create_time': now},
                       {'name': '赵六', 'age': 55, 'birth_date': '1968-10-08', 'sex': 0, 'grade': 1.0, 'point': 20.5, 'money': 854.56, 'create_time': now}])

    for u in Person.query():
        print(u)

    for u in Person.where(name='张三').select_page(2, 3):
        print(u)

    for u in Person.where(name='张三').find_page(2, 3):
        print(u)

    for u in Person.query_page(2,3):
        print(u)

    for u in Person.find_page(2,3):
        print(u)

    rowcount = Person.where(name='张三', age=55).delete()
    print(rowcount)

    cnt = Person.where(name='李四').count()
    print(cnt)

    for u in Person.where(name__like='王五%').query():
        print(u)

    Person.query(limit=(2, 3))

    print(Person.exists(name__eq='王五'))
    print(Person.find_one('id', 'name', name__eq='李四'))
    print(Person.select_one('id', 'name', name__eq='李四'))

    Person(id=621, name='lisi', age=44, del_flag=0).update(ignored_none=False)


def get_attr():
    person = Person(id=1, name='张三', age=41)
    person.logic_delete_by_id(1)


if __name__ == '__main__':
    db.init('test.db', driver='sqlite3', show_sql=True, debug=False)
    init_snowflake()
    # init_snowflake(epoch=int(time.mktime(time.strptime("2023-8-4 00:00:00", '%Y-%m-%d %H:%M:%S')) * 1000))
    _id = get_snowflake_id()
    print(_id, len(str(_id)))

    full_test()
    # print(Person.select(create_by__isnull=False))
    # print(Person.select(name__ne='王五'))
    # print(Person.select(age__range=(1, 100)))
    # print(Person.select(name__in=['王五','lisi']))
    # print(Person.select(name__not_in=['王五','lisi']))

    # import warnings
    # warnings.filterwarnings('ignore', category=DeprecationWarning)
    for p in Person.query(name='赵六'):
        print(p)

    for p in Person.fields('id', 'name', 'age').find(name__eq='李四'):
        print(p)
    for p in Person.fields('id', 'name', 'age').select(name__eq='赵六'):
        print(p)

    for p in Person.fields('id', 'name', 'age').find_by_ids(13073):
        print(p)

    for p in Person.select():
        print(p)

    for p in Person.fields('id', 'name', 'age').where(name__eq='赵六').page(1, 3).select():
        print(p)

    for p in Person.fields('id', 'name', 'age').page(1, 3).where(name__eq='赵六').select():
        print(p)

    for p in Person.page(1, 3).fields('id', 'name', 'age').where(name__eq='赵六').select():
        print(p)

    for p in Person.page(1, 3).where(name__eq='赵六').fields('id', 'name', 'age').select():
        print(p)

    for p in Person.where(name__eq='赵六').page(1, 3).fields('id', 'name', 'age').select():
        print(p)

    for p in Person.where(name__eq='赵六').fields('id', 'name', 'age').page(1, 3).find():
        print(p)

    print(Person.delete(name='王五'))
    print(Person.where(name='王五').delete())
    print(Person.fields('name').ravel_list())
    print(Person.fields('name').where(age=55).ravel_list())
    print(Person.count())
    print('exists_by_id', Person.exists_by_id(26))
