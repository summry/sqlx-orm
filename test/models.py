from decimal import Decimal
from datetime import date, datetime
from sqlormx import Model, KeyStrategy


class BaseModel(Model):
    __key__ = 'id'
    __del_flag__ = 'del_flag'
    __update_by__ = 'update_by'
    __update_time__ = 'update_time'
    __key_strategy__ = KeyStrategy.DB_AUTO_INCREMENT

    def __init__(self, id: int = None, create_by: int = None, create_time: datetime = None, update_by: int = None, update_time: datetime = None,
            del_flag: int = None):
        self.id = id
        self.create_by = create_by
        self.create_time = create_time
        self.update_by = update_by
        self.update_time = update_time
        self.del_flag = del_flag


class Person(BaseModel):
    # __table__ = 'person'
    # __select_key__ = "SELECT currval('person_id_seq')"

    def __init__(self, id: int = None, name: str = None, age: int = None, birth_date: date = None, sex: int = None, grade: float = None,
            point: float = None, money: Decimal = None, create_by: int = None, create_time: datetime = None, update_by: int = None,
            update_time: datetime = None, del_flag: int = None):
        super().__init__(id=id, create_by=create_by, create_time=create_time, update_by=update_by, update_time=update_time, del_flag=del_flag)
        self.name = name
        self.age = age
        self.birth_date = birth_date
        self.sex = sex
        self.grade = grade
        self.point = point
        self.money = money
