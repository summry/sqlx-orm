from decimal import Decimal
from dataclasses import dataclass
from datetime import date, datetime
from sqlormx import Model, db


@dataclass
class BaseModel(Model):
    id: int = None
    create_by: int = None
    create_time: datetime = None
    update_by: int = None
    update_time: datetime = None
    del_flag: int = None
    

@dataclass
class Person(BaseModel):
    __table__ = 'person'
    name: str = None
    age: int = None
    birth_date: date = None
    sex: int = None
    grade: float = None
    point: float = None
    money: Decimal = None
    

if __name__ == '__main__':
    db.init('test.db', driver='sqlite3', show_sql=True)
    u = Person(name='张三', age=55, birth_date='1968-10-08', sex=0, grade=1.0, point=20.5, money=854.56, create_time=datetime.now())
    rowcount = u.persist()
    print("rowcount: ", rowcount)
    
    p = Person.find_by_id(138)
    p.del_flag = 1
    p.update('del_flag')
    
    for p in Person.query(name='赵六'):
        print(p)

    