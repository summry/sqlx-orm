from sqlexecx.constant import CACHE_SIZE

NO_LIMIT = 0

MAPPER_PATH = 'mapper_path'

DEFAULT_KEY_FIELD = 'id'

KEY, SELECT_KEY, TABLE, UPDATE_BY, UPDATE_TIME, DEL_FLAG, KEY_STRATEGY = '__key__', '__select_key__', '__table__', \
                                                                         '__update_by__', '__update_time__',\
                                                                         '__del_flag__', '__key_strategy__'

KEY_SEQ = '__key_seq__'

